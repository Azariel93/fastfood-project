package fr.mds.fastfoodmealbuilder.item.drink;

public class Coke extends ColdDrink {

	@Override
	public String name() {
		return "Coke";
	}

	@Override
	public float price() {
		return 5;
	}
}
