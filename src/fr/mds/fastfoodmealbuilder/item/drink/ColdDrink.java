package fr.mds.fastfoodmealbuilder.item.drink;

import fr.mds.fastfoodmealbuilder.item.Item;
import fr.mds.fastfoodmealbuilder.packing.*;

public abstract class ColdDrink implements Item {
	
	@Override
	public Packing packing() {
		return new Bottle();
	}

}
