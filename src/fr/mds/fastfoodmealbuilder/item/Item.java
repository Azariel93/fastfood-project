package fr.mds.fastfoodmealbuilder.item;

import fr.mds.fastfoodmealbuilder.packing.Packing;

public interface Item {
	
	String name();
	Packing packing();
	float price();
	
}
