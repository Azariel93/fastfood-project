package fr.mds.fastfoodmealbuilder.item.food;

import fr.mds.fastfoodmealbuilder.item.Item;
import fr.mds.fastfoodmealbuilder.packing.*;

public abstract class Burger implements Item {

	@Override
	public Packing packing() {
		return new Wrapper();
	}
}
