package fr.mds.fastfoodmealbuilder.item.game;

import fr.mds.fastfoodmealbuilder.item.Item;
import fr.mds.fastfoodmealbuilder.packing.*;

public abstract class Game implements Item {

	@Override
	public Packing packing() {
		return new Bag();
	}

	@Override
	public float price() {
		return 0;
	}

}
