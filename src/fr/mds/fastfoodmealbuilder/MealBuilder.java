package fr.mds.fastfoodmealbuilder;

import fr.mds.fastfoodmealbuilder.item.Item;
import fr.mds.fastfoodmealbuilder.item.drink.*;
import fr.mds.fastfoodmealbuilder.item.food.*;
import fr.mds.fastfoodmealbuilder.item.game.*;

public class MealBuilder {

	private Meal meal = new Meal();
	
	public MealBuilder prepareVegMeal() {
		this.meal.addItem(new VegBurger());
		this.meal.addItem(new Coke());
		return this;
	}
	
	public MealBuilder prepareNonVegMeal() {
		this.meal.addItem(new ChickenBurger());
		this.meal.addItem(new Pepsi());
		return this;
	}
	
	public MealBuilder addItem(Item item) {
		this.meal.addItem(item);
		return this;
	}
	
	public Meal build() {
		Meal temp = this.meal;
		this.meal = new Meal();
		return temp;
	}
	
	public MealBuilder addGame() {
		if(Math.random() < 0.5) {
			this.meal.addItem(new DrawGame());
		} else {
			this.meal.addItem(new CarGame());
		}
		return this;
	}
	
}
