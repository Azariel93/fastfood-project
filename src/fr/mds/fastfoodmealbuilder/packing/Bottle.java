package fr.mds.fastfoodmealbuilder.packing;

public class Bottle implements Packing {

	@Override
	public String material() {
		return "Glass bottle";
	}

}
