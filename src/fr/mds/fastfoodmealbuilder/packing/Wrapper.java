package fr.mds.fastfoodmealbuilder.packing;

public class Wrapper implements Packing {

	@Override
	public String material() {
		return "Paper wrapper";
	}

}
