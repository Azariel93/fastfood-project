package fr.mds.fastfoodmealbuilder;

import fr.mds.fastfoodmealbuilder.item.drink.*;
import fr.mds.fastfoodmealbuilder.item.food.*;

public class BuilderPatternDemo {

	public static void main(String[] args) {
		final MealBuilder mb = new MealBuilder();

		Meal veg = mb.prepareVegMeal().build();
		System.out.println("  | Vegan meal |  ");
		veg.showItems();
		System.out.println("The cost is : " + veg.getCost() + "$");
		
		System.out.println();

		Meal Veg2 = mb.prepareVegMeal().addGame().build();
		System.out.println("  | Vegan kid meal |  ");
		Veg2.showItems();
		System.out.println("The cost is : " + Veg2.getCost() + "$");

		System.out.println();

		Meal nonVeg = mb.prepareNonVegMeal().build();
		System.out.println("  | Non vegan meal |  ");
		nonVeg.showItems();
		System.out.println("The cost is : " + nonVeg.getCost() + "$");
		
		System.out.println();

		Meal nonVeg2 = mb.prepareNonVegMeal().addGame().build();
		System.out.println("  | Non vegan kid meal |  ");
		nonVeg2.showItems();
		System.out.println("The cost is : " + nonVeg2.getCost() + "$");
		
		System.out.println();

		Meal custom = mb.prepareNonVegMeal().addItem(new ChickenBurger()).addItem(new Coke()).addGame().build();
		System.out.println("  | Custom meal |  ");
		custom.showItems();
		System.out.println("The cost is : " + custom.getCost() + "$");

	}
}
