package fr.mds.fastfoodmealbuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import fr.mds.fastfoodmealbuilder.item.Item;

public class Meal {
	
	private List<Item> itemList = new ArrayList<Item>();

	public void addItem (Item item) {
		itemList.add(item);
	}
	
	public float getCost() {
		float cost = 0;
		ListIterator<Item> itemIterator = itemList.listIterator();
		while(itemIterator.hasNext()){
			cost += itemIterator.next().price();
		}
//		for(Item i : itemList) {
//			cost += i.price();
//		}
		return cost;
	}
	
	public void showItems() {
		ListIterator<Item> itemIterator = itemList.listIterator();
		if (itemList.isEmpty()) {
			System.out.println("You don't have any items in your meal.");
		} else {
			System.out.println("Here is your meal:");
			while(itemIterator.hasNext()){
				Item item = itemIterator.next();
				System.out.println(" - " + item.name() + " (" + item.packing().material() + ")");
			}
		}
	}
}
