package fr.mds.player;

public class AudioPlayer implements MediaPlayer {

	@Override
	public void play(String format, String name) {
		if (format != "mp3") {
			MediaAdapter adapter = new MediaAdapter(format);
			adapter.play(format, name);
		} else {
			System.out.println("Playing audio file. Name: " + name);
		}
	}
}
