package fr.mds.player;

public interface AdvancedMediaPlayer {

	void playVlc(String fileName);

	void playMp4(String fileName);

}
