package fr.mds.player;

public class MediaAdapter implements MediaPlayer {
	
	AdvancedMediaPlayer audioPlayer;

	public MediaAdapter(String format) {
		if (format.equalsIgnoreCase("mp4")) {
			audioPlayer = new Mp4Player();
		} else if (format.equalsIgnoreCase("vlc")) {
			audioPlayer = new VlcPlayer();
		} else {
			System.out.println("Unsupported format.");
		}
	}

	@Override
	public void play(String format, String name) {
		if (format.equalsIgnoreCase("mp4")) {
			audioPlayer.playMp4(name);
		} else if (format.equalsIgnoreCase("vlc")) {
			audioPlayer.playVlc(name);
		}
	}

}
