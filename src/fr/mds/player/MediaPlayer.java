package fr.mds.player;

public interface MediaPlayer {

	void play(String format, String name);

}
